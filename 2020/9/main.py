numbers_file = open("input", "r")

answers = list(map(lambda x: int(x), numbers_file.read().splitlines()))

preamble = 25


def aoc_1(preamble, answers):
    for index in range(len(answers) - preamble):
        nums = []
        for i in answers[index:index + preamble]:
            for j in answers[index:index + preamble]:
                nums.append(i + j)

        if answers[index + preamble] not in nums:
            return answers[index + preamble], index + preamble


aoc, index = aoc_1(preamble, answers)
#print(aoc)

i = 0
while i < index:
    encrypt = 0
    j = i
    nums = []
    while encrypt < aoc:
        nums.append(answers[j])
        encrypt += answers[j]
        j += 1
        if encrypt == aoc:
            print(max(nums)+min(nums))
    i += 1
