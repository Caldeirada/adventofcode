bags_file = open("input", "r")
bags_dict = {bag.split(" contain ")[0]: bag.split(" contain ")[1].split(", ") for bag in
             bags_file.read().replace(" bags", "").replace(" bag", "").replace(".", "").splitlines()}

SHNY = "shiny gold"
NO_MORE = "no other"

count = 0


def can_hold_shiny(bags):
    if SHNY in "".join(bags):
        return 1
    elif NO_MORE in bags:
        return 0
    else:
        for bag in bags:
            if can_hold_shiny(bags_dict[bag[2:]]) == 1:
                return 1
        return 0


def get_dept(bags):
    if NO_MORE in bags:
        return 0
    else:
        dept = 0
        for bag in bags:
            dept += int(bag[0]) + int(bag[0]) * get_dept(bags_dict[bag[2:]])
        return dept


for bag in bags_dict.keys():
    count += can_hold_shiny(bags_dict[bag])

print(count)
print(get_dept(bags_dict[SHNY]))
