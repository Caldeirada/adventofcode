passwords_file = open("input", "r")

passwords = passwords_file.read().splitlines()

passwords = [x.replace(":"," ").replace("-", " ").split() for x in passwords]

valid_1 = 0
valid_2 = 0

# find [min, max, char, str] count lines that match number of times char is in str
for toboggan in passwords:
    min_char = int(toboggan[0])
    max_char = int((toboggan[1]))
    char = toboggan [2]
    password = toboggan [3]

    approval = sum(map(lambda x: 1 if char in x else 0, password))
    if min_char <= approval <= max_char:
        valid_1+=1


print(valid_1)

# find [min, max, char, str] count lines that match number of times char is in str
for toboggan in passwords:
    index_1 = int(toboggan[0])-1
    index_2 = int((toboggan[1]))-1
    char = toboggan[2]
    password = toboggan[3]

    if not (index_1 > len(password) or index_1 > len(password)):
        test = {password[index_1], password[index_2]}

        if char in test and len(test)>1:
            valid_2 += 1

print(valid_2)
