game_file = open("input", "r")

game = [action.split(" ") for action in game_file.read().splitlines()]

NOP = "nop"
ACC = "acc"
JMP = "jmp"


def run(action_list):
    taken_actions = set([])
    accumulator = 0
    action = 0

    while action < len(action_list):
        if action in taken_actions:
            return accumulator, False

        taken_actions.add(action)

        if action_list[action][0] == NOP:
            action += 1
        elif action_list[action][0] == ACC:
            accumulator += int(action_list[action][1])
            action += 1
        elif action_list[action][0] == JMP:
            action += int(action_list[action][1])

    print("FIXED")
    return accumulator, True


print(run(game))

for action in game:
    if action[0] == NOP:
        action[0] = JMP
        acc, suc = run(game)
        if suc:
            print(action)
            print(acc)
        action[0] = NOP
    elif action[0] == JMP:
        action[0] = NOP
        acc, suc = run(game)
        if suc:
            print(action)
            print(acc)
        action[0] = JMP
