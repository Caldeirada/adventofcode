seats_file = open("input", "r")

seats_map = seats_file.read().splitlines()
heigth = len(seats_map)
width = len(seats_map[0])


def has_adjacent(i, j, seats):
    count = 0
    if i == 0:
        x_range = [0, 1]
    elif i == heigth - 1:
        x_range = [-1, 0]
    else:
        x_range = [-1, 0, 1]

    if j == 0:
        y_range = [0, 1]
    elif j == width - 1:
        y_range = [-1, 0]
    else:
        y_range = [-1, 0, 1]

    for x in x_range:
        for y in y_range:
            if not (x == 0 and y == 0):
                if seats[i + x][j + y] == "#":
                    count += 1
    return count


def has_adjacent_new(i, j, seats):
    count = 0

    up = True
    down = True
    for x in range(1, heigth):
        if not up and not down:
            break
        if seats[min(i + x, heigth - 1)][j] == "#" and up:
            if min(i + x, heigth - 1) != i:
                count += 1
                up = False
        elif seats[min(i + x, heigth - 1)][j] == "L":
            if min(i + x, heigth - 1) != i:
                up = False
        if seats[max(i - x, 0)][j] == "#" and down:
            if max(i - x, 0) != i:
                count += 1
                down = False
        elif seats[max(i - x, 0)][j] == "L":
            if max(i - x, 0) != i:
                down = False

    up = True
    down = True
    for y in range(1, width):
        if not up and not down:
            break
        if seats[i][min(j + y, width - 1)] == "#" and up:
            if min(j + y, width - 1) != j:
                count += 1
                up = False
        elif seats[i][min(j + y, width - 1)] == "L":
            if min(j + y, width - 1) != j:
                up = False
        if seats[i][max(j - y, 0)] == "#" and down:
            if max(j - y, 0) != j:
                count += 1
                down = False
        elif seats[i][max(j - y, 0)] == "L":
            if max(j - y, 0) != j:
                down = False

    up = True
    down = True
    left = True
    right = True
    for x, y in zip(range(1, heigth), range(1, width)):
        if not up and not down and not left and not right:
            break
        if seats[min(i + x, heigth - 1)][min(j + y, width - 1)] == "#" and up:
            if min(i + x, heigth - 1) != i and min(j + y, width - 1) != j:
                count += 1
                up = False
        elif seats[min(i + x, heigth - 1)][min(j + y, width - 1)] == "L":
            up = False
        else:
            if min(i + x, heigth - 1) == heigth - 1 or min(j + y, width - 1) == width - 1:
                up = False
        if seats[max(i - x, 0)][max(j - y, 0)] == "#" and down:
            if max(i - x, 0) != i and max(j - y, 0) != j:
                count += 1
                down = False
        elif seats[max(i - x, 0)][max(j - y, 0)] == "L":
            down = False
        else:
            if max(i - x, 0) == 0 or max(j - y, 0) == 0:
                down = False
        if seats[max(i - x, 0)][min(j + y, width - 1)] == "#" and left:
            if max(i - x, 0) != i and min(j + y, width - 1) != j:
                count += 1
                left = False
        elif seats[max(i - x, 0)][min(j + y, width - 1)] == "L":
            left = False
        else:
            if max(i - x, 0) == 0 or min(j + y, width - 1) == width - 1:
                left = False
        if seats[min(i + x, heigth - 1)][max(j - y, 0)] == "#" and right:
            if min(i + x, heigth - 1) != i and max(j - y, 0) != j:
                count += 1
                right = False
        elif seats[min(i + x, heigth - 1)][max(j - y, 0)] == "L":
            right = False
        else:
            if min(i + x, heigth - 1) == heigth - 1 or max(j - y, 0) == 0:
                right = False
    return count


def iter(seats):
    matrix = []
    for i in range(heigth):
        line = []
        for j in range(width):
            if seats[i][j] == ".":
                line.append(".")
            elif seats[i][j] == "L":
                c = has_adjacent(i, j, seats)
                if c == 0:
                    line.append("#")
                else:
                    line.append("L")
            elif seats[i][j] == "#":
                c = has_adjacent(i, j, seats)
                if c < 4:
                    line.append("#")
                else:
                    line.append("L")
        matrix.append("".join(line))

    return matrix


def iter_new(seats):
    matrix = []
    for i in range(heigth):
        line = []
        for j in range(width):
            if seats[i][j] == ".":
                line.append(".")
            elif seats[i][j] == "L":
                c = has_adjacent_new(i, j, seats)
                if c == 0:
                    line.append("#")
                else:
                    line.append("L")
            elif seats[i][j] == "#":
                c = has_adjacent_new(i, j, seats)
                if c < 5:
                    line.append("#")
                else:
                    line.append("L")
        matrix.append("".join(line))

    return matrix


def pp(l):
    count = 0
    for i in l:
        print(i)
        for j in i:
            if j == "#":
                count += 1

    print(count)


prev = []
next = iter(seats_map)
while prev != next:
    prev = next
    next = iter(prev)
print(pp(next))
prev_new = []
next_new = iter_new(seats_map)
while prev_new != next_new:
   prev_new = next_new
   next_new = iter_new(prev_new)
print(pp(next_new))
