import re

passports_file = open("input", "r")

passports = passports_file.read().replace("\n", ",").replace(",,", "\n").replace(",", " ").split("\n")
valid = 0

regex_byr = re.compile("byr:((19[2-9][0-9])|(200[0-2]))")
regex_iyr = re.compile("iyr:20((1[0-9])|20)")
regex_eyr = re.compile("eyr:20((2[0-9])|30)")
regex_hgt = re.compile("hgt:((1[5-8][0-9]|19[0-3])cm|(59|6[0-9]|7[0-6])in)")
regex_hcl = re.compile("hcl:#([0-9a-f]{6})")
regex_ecl = re.compile("ecl:(amb|blu|brn|gry|grn|hzl|oth)")
regex_pid = re.compile("pid:([0-9]{9})")
regex = [regex_byr, regex_iyr, regex_eyr, regex_hgt, regex_hcl, regex_ecl, regex_pid]


def validate_passport(passport):
    for reg in regex:
        if reg.search(passport) is None:
            return 0

    return 1


for passport in passports:
    if len(passport.split(" ")) == 8:
        valid += validate_passport(passport)
    elif (len(passport.split(" ")) == 7) and (not ("cid:" in passport)):
        valid += validate_passport(passport)

print(valid)
