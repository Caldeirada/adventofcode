tickets_file = open("input", "r")

tickets = tickets_file.read().splitlines()


def bin_to_dec(number):
    dec = 0
    for i in range(len(number)):
        dec += int(number[-(1 + i)]) * (2 ** i)

    return dec


max_seat_id = 0
seat_ids = []
for ticket in tickets:
    row = bin_to_dec(ticket[:-3].replace("F", "0").replace("B", "1"))
    column = bin_to_dec(ticket[-3:].replace("L", "0").replace("R", "1"))

    seat_id = row * 8 + column
    seat_ids.append(seat_id)
    if seat_id > max_seat_id:
        max_seat_id = seat_id

print(max_seat_id)
seat_ids = list(set(seat_ids))

for seat in range(len(seat_ids) - 1):
    if seat_ids[seat + 1] - seat_ids[seat] > 1:
        print(seat_ids[seat + 1], seat_ids[seat])
