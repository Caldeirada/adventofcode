def list_mult(mult):
    if len(mult) == 0:
        return 1
    else:
        return mult[0] * list_mult(mult[1:])


map_file = open("input", "r")

sled_map = map_file.read().splitlines()
WIDTH = len(sled_map[0])

# list of test movement [right, down]
movements = [[1, 1], [3, 1], [5, 1], [7, 1], [1, 2]]
trees = [0, 0, 0, 0, 0]
tree_ind = 0
for movement in movements:
    index_r = 0
    index_d = 0

    right = movement[0]
    down = movement[1]
    while index_d < len(sled_map):
        if sled_map[index_d][index_r] == "#":
            trees[tree_ind] += 1
        index_r = (index_r + right) % WIDTH
        index_d += down

    tree_ind += 1

print(trees, list_mult(trees))
