answers_file = open("input", "r")

answers = answers_file.read().replace("\n", ",").replace(",,", "\n").replace(",", " ").split("\n")

sum_answers = 0
sum_answers_2 = 0

letters = {'k', 'q', 'c', 'b', 'd', 'z', 'r', 'p', 'v', 'g', 's', 'f', 'h', 'l', 'a', 'i', 'w', 'm', 'n', 'u', 'j', 'o',
           'y', 'x', 't', 'e'}

for answer in answers:
    sum_answers += len(set(answer.replace(" ", "")))

    sets = [set(ans) for ans in answer.split(" ")]
    result = sets[0]
    for ans in sets[1:]:
        result = ans & result

    sum_answers_2 += len(result)

print(sum_answers)
print(sum_answers_2)
