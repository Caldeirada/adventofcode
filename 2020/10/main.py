import functools

joltages_file = open("input", "r")

joltage = [0] + list(set(map(lambda x: int(x), joltages_file.read().splitlines())))
joltage.append(max(joltage) + 3)

ones = 0
threes = 0

for i in range(len(joltage) - 1):
    jolt = joltage[i + 1] - joltage[i]
    if jolt == 1:
        ones += 1
    elif jolt == 3:
        threes += 1

aux = {}
total_count = 1
for i in joltage:
    count = []
    for j in range(1, 4):
        if i - j in joltage:
            count.append(i - j)

    aux[i] = count

print(aux)

aux_d = {}
result = []
counter = 0


def get_prev(seq):
    res = 0
    if len(seq) == 0:
        res = 1
    else:
        for v in seq:
            if v in aux_d.keys():
                prev = aux_d[v]
            else:
                prev = get_prev(aux[v])
                aux_d[v] = prev
            res += prev
    return res


print(get_prev(aux[joltage[-2]]))

print(result)
print(ones * threes)

c = 0

for i in result:
    if i == 1:
        c += 1

print(c)
