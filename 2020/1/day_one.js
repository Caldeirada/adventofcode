//file system lib
const fs = require("fs");

/*
problem: given a list find 2 value that sum 2020 and return their product
*/
function report_1(expense_reports) {
  for (var i = 0; i < expense_reports.length; i++) {
    report = 2020 - expense_reports[i];
    if (expense_reports.includes(report)) {
      return [expense_reports[i], report, expense_reports[i] * report];
    }
  }

  /*
  alternative
  return expense_reports
    .map((report) => expense_reports.indexOf(2020 - report))
    .filter((report) => report > 0)
    .map((num) => expense_reports[num]);
  */
}

/*
problem: given a list find 3 value that sum 2020 and return their product
*/
function report_2(expense_reports) {
  let new_reports = expense_reports.map((report) => 2020 - report);

  for (var i = 0; i < expense_reports.length; i++) {
    for (var j = 0; j < expense_reports.length; j++) {
      report = expense_reports[i] + expense_reports[j];
      if (new_reports.includes(report)) {
        return [
          expense_reports[i],
          expense_reports[j],
          2020 - (expense_reports[i] + expense_reports[j]),
          expense_reports[i] *
            expense_reports[j] *
            (2020 - (expense_reports[i] + expense_reports[j])),
        ];
      }
    }
  }
}

//read file
const data = fs.readFileSync("input", "UTF-8");

// split the contents by new line and convert to int
const expense_reports = data.split(/\r?\n/).map((report) => parseInt(report));

console.log(report_1(expense_reports));
console.log(report_2(expense_reports));
