
def reports_2(expense_reports):
    for index in range(len(expense_reports)):
        report = 2020 - expense_reports[index]
        if report in expense_reports:
            return report, expense_reports[index]

def reports_3(expense_reports):
    expense_reports_3 = [2020 - x for x in expense_reports]

    for i in range(len(expense_reports)):
        for j in range(len(expense_reports)):
            report = expense_reports[i] + expense_reports[j]
            if report in expense_reports_3:
                return expense_reports[i], expense_reports[j], (2020 - (expense_reports[i] + expense_reports[j]))


reports = open("input", "r")

expense_reports = []

for report in reports.readlines():
    expense_reports.append(int(report))

r1, r2 = reports_2(expense_reports)
r3, r4, r5 = reports_3(expense_reports)

print(f"{r1}, {r2}; with product {r1*r2}")
print(f"{r3}, {r4}, {r5}; with product {r3*r4*r5}")
