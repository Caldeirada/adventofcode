open = io.open

function read_file(path)
    local f = assert(io.open(path, "rb"))
    local content = f:read("*all")
    f:close()
    return content
end

function split(str, sep)
   local result = {}
   local regex = ("([^%s]+)"):format(sep)
   for each in str:gmatch(regex) do
      table.insert(result, each)
   end
   return result
end

stacks_crane_file = read_file("input.txt");
stacks_crane = split(stacks_crane_file, "\n")

function print_stacks(stacks)
    for k,v in pairs(stacks) do
        print(k, #v)
    end
end

stacks = {}
crane = {}

for k,l in pairs(stacks_crane) do
    if l:match("move") then
        m = {}
        for n in l:gmatch("[0-9]+") do
            table.insert(m,tonumber(n))
        end
        table.insert(crane, m)
    else
        s = {}
        for i=1,math.floor(#l/3) do
            value = l:sub(1+3*(i-1)+(i-1),3+3*(i-1)+(i-1)):match("[A-Z]")
            if value ~= nil then
                if stacks[i] == nil then
                    stacks[i] = {}
                end
                table.insert(stacks[i], value)
            end
        end
    end
end

function top_crates(stacks)
    i = ""
    for k,l in pairs(stacks) do
        i = i .. l[1]
    end
    print(i)
end

function cratemover9000(crane, stacks)
    for _,l in pairs(crane) do
        num_crates = l[1]
        from = l[2]
        to_stack = l[3]
        for i=1,num_crates do
            s = table.remove(stacks[from], 1)
            if s ~= nil then
                table.insert(stacks[to_stack], 1, s)
            end
        end
    end
    top_crates(stacks)
end

function cratemover9001(crane, stacks)
    for _,l in pairs(crane) do
        num_crates = l[1]
        from = l[2]
        to_stack = l[3]
        for i=math.min(num_crates, #stacks[from]),1,-1 do
            s = table.remove(stacks[from], i)
            if s ~= nil then
                table.insert(stacks[to_stack], 1, s)
            end
        end
    end
    top_crates(stacks)
end

--cratemover9000(crane,stacks)
cratemover9001(crane,stacks)
