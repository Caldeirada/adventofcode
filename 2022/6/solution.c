#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

bool is_start_marker(char* marker, int length) {
	for (int i = 0; i < length; i++) {
		for (int j = i+1; j < length; j++) {
			if (marker[j] == marker[i]) {
				return false;
			}
		}
	}
	return true;
}


int main(int argc, char* argv[])
{
	FILE* fptr1;
	char c;

	char marker[4];
	char packet[14];
	int i = 0, j, from, to;
    bool check_marker, check_packet;

	fptr1 = fopen(argv[1], "r");

	if (fptr1 == NULL) {
		return 1;
	}

	for (i = 0; c != EOF; i++) {
		c = fgetc(fptr1);
		if (i < 4) {
			marker[i] = c;
		} else if(!check_marker) {
            check_marker = is_start_marker(marker, 4);
            if (check_marker) {                
                printf("start marker: %d\n",i);
            }
			for (int j = 0; j < 3; j++) {
				marker[j] = marker[j+1];	
			}
			marker[3] = c;
        }

		if (i < 14) {
			packet[i] = c;
		} else {
			check_packet = is_start_marker(packet, 14);
            if (check_packet) {                
                printf("start packet: %d\n",i);
				return 1;
            }
			for (int j = 0; j < 13; j++) {
				packet[j] = packet[j+1];	
			}
			packet[13] = c;
		}
	}
	fclose(fptr1);

	return 0;
}