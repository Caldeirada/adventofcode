import java.io.File

val sacks: List<String> = File("input").readLines()

val common = sacks.map { it.chunked(it.length/2)[0].fold('o', { c: Char, s: Char -> if (it.chunked(it.length/2)[1].contains(s)) s else c }) }
val values = common.fold(0, { s:Int , n: Char -> if (n.isUpperCase()) s+(n.code - 38) else s+(n.code - 96) })
println(common)
println(values)
val groups = sacks.chunked(3).map { it[0].fold('a', {c:Char, s:Char -> if(it[1].contains(s)&&it[2].contains(s)) s else c})}
val values2 = groups.fold(0, { s:Int , n: Char -> if (n.isUpperCase()) s+(n.code - 38) else s+(n.code - 96) })
println(groups)
println(values2)
