package main

import (
    "fmt"
    "io/ioutil"
    "strings"
    "strconv"
)

func main() {
    content, err := ioutil.ReadFile("input")
    if err != nil {
        fmt.Println("Err")
    }
    assignments := convert_int(string(content))
    count := 0
    count2 := 0
    for _, value := range assignments {
        if (value[0][0] <= value[1][0] && value[0][1] >= value[1][1]) {
            count = count + 1
            count2 = count2 + 1
        } else if (value[0][0] >= value[1][0] && value[0][1] <= value[1][1])  {
            count = count + 1
            count2 = count2 + 1
        } else if (value[0][0] == value[1][1] || value[0][1] == value[1][0] || value[0][0] == value[1][0] || value[0][1] == value[1][1]) {
            count2 = count2 + 1
        } else if (value[0][0] <= value[1][1] && value[0][1] >= value[1][0]) {
            count2 = count2 + 1
        } else if (value[0][0] >= value[1][1] && value[0][1] <= value[1][0]) {
            count2 = count2 + 1
        }
        //fmt.Println(value)
    }
    fmt.Printf("Count 1: %d\n",count)
    fmt.Printf("Count 2: %d\n",count2)
}

func convert_int(assignment_string string) [][2][2]int {
    assignments := strings.Split(assignment_string, "\n")
    var l1 [][2][2]int
    for _, assignment := range assignments {
        a := strings.Split(assignment, ",")
        var l2 [2][2]int
        for k, value := range a {
            tasks := strings.Split(value, "-")
            //fmt.Println(tasks)
            i1, _ := strconv.Atoi(tasks[0])
            i2, _ := strconv.Atoi(tasks[1])
            t := [2]int {i1, i2}
            l2[k] = t
        }
        l1 = append(l1, l2)
    }
    return l1
}
