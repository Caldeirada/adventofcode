#!/usr/bin/env python3
import sys

input = "input"
if len(sys.argv) > 1:
    input = sys.argv[1]


elfs = {0:[]}

with open(input) as elfs_packages:
    i_elf = 0
    packages = elfs_packages.readlines()
    for package in packages:
        if len(package.strip()) == 0:
            i_elf += 1
            elfs[i_elf] = []
        else:
            elfs[i_elf].append(int(package.strip()))


elfs_calories = {}
for elf, package in elfs.items():
    elfs_calories[elf] = sum(package)

print(f"Max calories: {max(elfs_calories.values())}")

print(f"Sum top 3 calories: {sum(sorted(elfs_calories.values())[-3:])}")
