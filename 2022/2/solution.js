const fs = require("fs");

const elf = {
  "A": 1,//"rock",
  "B": 2,//"paper",
  "C": 3,//"scissors"
}


const player = {
  "X": 1,//"rock",
  "Y": 2,//"paper",
  "Z": 3,//"scissors"
}

const player_2 = {
  "X": -1,//"rock",
  "Y": 0,//"paper",
  "Z": 1,//"scissors"
}


//read file
const data = fs.readFileSync("input", "UTF-8");

// split the contents by new line and convert to int
const rock_paper_scissors = data.split(/\n+/g).map((game) => game.split(" "));
rock_paper_scissors.pop()
const winner = (p1, p2) => p1 % 3 === p2-1 ? 0 : p1 === p2 ? 3 : 6
const get_play = (game) => ((((elf[game[0]]-1+player_2[game[1]]) % 3) + 3) % 3) + 1
const score = (score_acc, game) => [score_acc[0]+elf[game[0]] + winner(elf[game[0]], player[game[1]]), score_acc[1]+player[game[1]]+winner(player[game[1]], elf[game[0]])]
console.log(rock_paper_scissors.reduce(score, [0,0]))
const score_2 = (score_acc, game) => [score_acc[0]+elf[game[0]] + winner(elf[game[0]], get_play(game)), score_acc[1]+get_play(game)+winner(get_play(game), elf[game[0]])]
console.log(rock_paper_scissors.reduce(score_2, [0,0]))
//console.log(rock_paper_scissors)
